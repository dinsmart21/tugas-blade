@extends('layout.master')
@section('judul')
Buat Account Baru!
@endsection
    
@section('content')
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
        @csrf

        <label for="fname">First name:</label><br /><br />
        <input type="text" id="fname" name="fname" value="" /><br /><br />
        <label for="lname">Last name:</label><br /><br />
        <input type="text" id="lname" name="lname" value="" /><br /><br />

        <p>Gender:</p>
         <input type="radio" name="Gender" /> Male <br />
         <input type="radio" name="Gender" /> Female <br />
         <input type="radio" name="Gender" /> Other <br />

        <p>Nationality:</p>
        <select>
            <option>Indonesia</option>
            <option>Amerika</option>
            <option>Inggris</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" /> Bahasa Indonesia<br />
        <input type="checkbox" /> English<br />
        <input type="checkbox" /> Other<br />

        <p>Bio:</p>
        <textarea name="" id="" cols="50" rows="10"></textarea><br />

        <input type="submit" value=" Sign Up" />
    </form>
    @endsection